const tempToken = ` eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImhhbXplbG8uYWxpQGdtYWlsLmNvbSIsInR5cGUiOiJ0ZW1wIiwiaWF0IjoxNzEzNDM1NjgzLCJleHAiOjE3MTM0MzYyODN9.H8QXemttUjfhuZqJRmQHOQvEteh_4vxZr30FRZF-xiE`;

export class Api {
  constructor(baseUrl) {
    this.baseUrl = baseUrl;
    this.token = tempToken ?? null;
  }

  // Method to fetch the token
  async fetchToken(endpoint, credentials) {
    const response = await fetch(`${this.baseUrl}${endpoint}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(credentials),
    });
    const data = await response.json();
    if (data.token) {
      this.token = data.token;
    }
    return data;
  }

  async post(endpoint, data) {
    const response = await fetch(`${this.baseUrl}${endpoint}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        ...(this.token ? { Authorization: `Bearer ${this.token}` } : {}),
      },
      body: JSON.stringify(data),
    });
    return response.json();
  }

  async get(endpoint) {
    const response = await fetch(`${this.baseUrl}${endpoint}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        ...(this.token ? { Authorization: `Bearer ${this.token}` } : {}),
      },
    });
    return response.json();
  }

  async put(endpoint, data) {
    const response = await fetch(`${this.baseUrl}${endpoint}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        ...(this.token ? { Authorization: `Bearer ${this.token}` } : {}),
      },
      body: JSON.stringify(data),
    });
    return response.json();
  }

  async patch(endpoint, data) {
    const response = await fetch(`${this.baseUrl}${endpoint}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        ...(this.token ? { Authorization: `Bearer ${this.token}` } : {}),
      },
      body: JSON.stringify(data),
    });
    return response.json();
  }

  async delete(endpoint) {
    const response = await fetch(`${this.baseUrl}${endpoint}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        ...(this.token ? { Authorization: `Bearer ${this.token}` } : {}),
      },
    });
    return response.json();
  }
}

//useage
// Initialize the API class with the base URL
export const api = new Api("https://gbackend.liara.run/v1");

// // Function to fetch the token
// async function fetchTokenAndUse() {
//  try {
//     // Fetch the token
//     const tokenResponse = await api.fetchToken('/auth/token', {
//       username: 'your_username',
//       password: 'your_password'
//     });

//     // Check if the token was successfully fetched
//     if (tokenResponse.success) {
//       // Now you can use the token for other API calls
//       const userResults = await api.get('/user/previous-results');
//       console.log(userResults);
//     } else {
//       console.error('Failed to fetch token');
//     }
//  } catch (error) {
//     console.error('Error:', error);
//  }
// }

// // Call the function to fetch the token and use it for an API call
// fetchTokenAndUse();

export async function getWinnersApi() {
  try {
    const results = await api.get("/game/winners");
    return results;
  } catch (error) {
    console.error("Error fetching user previous results:", error);
  }
}

export async function getLoadApi() {
  try {
    const results = await api.get("/game/load");
    return results;
  } catch (error) {
    console.error("Error fetching user previous results:", error);
  }
}

export async function getRollApi() {
  try {
    const results = await api.get("/game/roll");
    return results;
  } catch (error) {
    console.error("Error fetching user previous results:", error);
  }
}

export async function getWinnersApi() {
  try {
    const results = await api.get("/game/winners");
    return results;
  } catch (error) {
    console.error("Error fetching user previous results:", error);
  }
}

export async function updateUserApi(data) {
  try {
    const results = await api.put("/game/winners", data);
    return results;
  } catch (error) {
    console.error("Error fetching user previous results:", error);
  }
}
