var gulp = require("gulp");
var browserSync = require("browser-sync").create();

// Task to start BrowserSync
gulp.task("browserSync", function () {
  browserSync.init({
    server: {
      baseDir: "./", // Set the base directory for BrowserSync
    },
  });
});

// Task to watch for changes in CSS files and reload the browser
gulp.task("watch", function () {
  gulp.watch(
    "*.css",
    gulp.series("browserSync", function (done) {
      browserSync.reload();
      done();
    })
  );
});

// Default task to run both BrowserSync and watch tasks
gulp.task("default", gulp.series("browserSync", "watch"));
