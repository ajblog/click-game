const accessToken =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEyZDk0NzhkLTliY2MtNDRkOS04ZDdjLWQxZTkxYjIwNmQ2MSIsInJvbGUiOiJVU0VSIiwibmFtZSI6ImFsaSIsImVtYWlsIjoiaGFtemVsby5hbGlAZ21haWwuY29tIiwiaWF0IjoxNzEzNDM1NzEyLCJleHAiOjE3NDQ5NzE3MTJ9._MDoJ39yZs_2T4qxdQhiziN1WYEwsHttDVta5N13EyI";

class Api {
  constructor(baseUrl) {
    this.baseUrl = baseUrl;
    this.token = accessToken ? accessToken : null;
  }

  // Method to fetch the token
  async fetchToken(endpoint, credentials) {
    const response = await fetch(`${this.baseUrl}${endpoint}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(credentials),
    });
    const data = await response.json();
    if (data.token) {
      this.token = data.token;
    }
    return data;
  }

  async post(endpoint, data) {
    const response = await fetch(`${this.baseUrl}${endpoint}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        ...(this.token ? { Authorization: `Bearer ${this.token}` } : {}),
      },
      body: JSON.stringify(data),
    });
    return response.json();
  }

  async get(endpoint) {
    const response = await fetch(`${this.baseUrl}${endpoint}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        ...(this.token ? { Authorization: `Bearer ${this.token}` } : {}),
      },
    });
    return response.json();
  }

  async put(endpoint, data) {
    const response = await fetch(`${this.baseUrl}${endpoint}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        ...(this.token ? { Authorization: `Bearer ${this.token}` } : {}),
      },
      body: JSON.stringify(data),
    });
    return response.json();
  }

  async patch(endpoint, data) {
    const response = await fetch(`${this.baseUrl}${endpoint}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        ...(this.token ? { Authorization: `Bearer ${this.token}` } : {}),
      },
      body: JSON.stringify(data),
    });
    return response.json();
  }

  async delete(endpoint) {
    const response = await fetch(`${this.baseUrl}${endpoint}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        ...(this.token ? { Authorization: `Bearer ${this.token}` } : {}),
      },
    });
    return response.json();
  }
}

//useage
// Initialize the API class with the base URL
const api = new Api("https://gbackend.liara.run/v1");

// // Function to fetch the token
// async function fetchTokenAndUse() {
//  try {
//     // Fetch the token
//     const tokenResponse = await api.fetchToken('/auth/token', {
//       username: 'your_username',
//       password: 'your_password'
//     });

//     // Check if the token was successfully fetched
//     if (tokenResponse.success) {
//       // Now you can use the token for other API calls
//       const userResults = await api.get('/user/previous-results');
//       console.log(userResults);
//     } else {
//       console.error('Failed to fetch token');
//     }
//  } catch (error) {
//     console.error('Error:', error);
//  }
// }

// // Call the function to fetch the token and use it for an API call
// fetchTokenAndUse();

async function getWinnersApi() {
  try {
    const results = await api.get("/game/winners");
    return results;
  } catch (error) {
    console.error("Error fetching user previous results:", error);
  }
}

async function getLoadApi() {
  try {
    const results = await api.get("/game/load");
    return results;
  } catch (error) {
    console.error("Error fetching user previous results:", error);
  }
}

async function getRollApi() {
  try {
    const results = await api.get("/game/roll");
    return results;
  } catch (error) {
    console.error("Error fetching user previous results:", error);
  }
}

async function getWinnersApi() {
  try {
    const results = await api.get("/game/winners");
    return results;
  } catch (error) {
    console.error("Error fetching user previous results:", error);
  }
}

async function updateUserApi(data) {
  try {
    const results = await api.put("/user", data);
    return results;
  } catch (error) {
    console.error("Error fetching user previous results:", error);
  }
}

let appState = "home";
let chars = document.getElementsByClassName("character");
let selectedEmoji = document.getElementsByClassName("emoji");
let selectEmojiContainer = document.getElementsByClassName(
  "emoji-picker-container"
);
let allEmojis = document.getElementsByClassName("emoji-select");
let isDropDownOpen = false;

let greenDice = document.getElementsByClassName("dice1");
let yellowDice = document.getElementsByClassName("dice2");
const yesAudio = new Audio("./assets/Sounds/yes1.wav");
const noAudio = new Audio("./assets/Sounds/no1.wav");
const userScore = document.getElementsByClassName("point-text")[0];

getLoadApi().then((loadResult) => {
  userScore.textContent = loadResult.result.score;
  for (let i = 0; i < selectedEmoji.length; i++) {
    selectedEmoji[
      i
    ].src = `./assets/Graphics/EmojiSheet/EmojiOne_${loadResult.result.emoji}.png`;
  }
});

getWinnersApi().then((loadResult) => {
  let winners = loadResult.result.winners;
  winners.sort((a, b) => a.rank - b.rank);

  document.getElementsByClassName("point-item")[0].innerHTML = `
  ${winners.map(
    (item, index) => `
  <p class="score">${item.point}</p>
  <p class="username">${item.name}</p>
  <p class="rank ${
    index === 0 ? "first" : index === 1 ? "second" : index === 2 ? "third" : ""
  }">${index + 1}</p>
  `
  )}`;
});

selectedEmoji[0].addEventListener("click", () => {
  isDropDownOpen = !isDropDownOpen;
  if (isDropDownOpen) selectEmojiContainer[0].style.display = "flex";
  else if (!isDropDownOpen) selectEmojiContainer[0].style.display = "none";
});

Array.from(allEmojis).forEach((item, index) => {
  item.addEventListener("click", async () => {
    await updateUserApi({
      emoji: index + 1,
    });
    for (let i = 0; i < selectedEmoji.length; i++) {
      selectedEmoji[i].src = `./assets/Graphics/EmojiSheet/EmojiOne_${
        index + 1
      }.png`;
    }
    isDropDownOpen = false;
    selectEmojiContainer[0].style.display = "none";
  });
});

function playAudio(audio) {
  audio.play();
}

const pointsContainer = document
  .getElementsByClassName("points-container")[0]
  .addEventListener("click", () => {
    appState = "board";
    setAppState();
  });

const backIcon = document
  .getElementsByClassName("back-icon")[0]
  .addEventListener("click", () => {
    appState = "home";
    setAppState();
  });

const setAppState = () => {
  if (appState === "home") {
    document.getElementsByClassName("background-container")[1].style.display =
      "none";
    document.getElementsByClassName("background-container")[0].style.display =
      "flex";
  } else {
    document.getElementsByClassName("background-container")[1].style.display =
      "flex";
    document.getElementsByClassName("background-container")[0].style.display =
      "none";
  }
};

setAppState();

for (let i = 0; i < chars.length; i++) {
  chars[i].src = "./assets/Graphics/TrumpYesSheet/trumpYesSpriteSheet_1.png";
}

function getRandomDiceNumber() {
  return Math.floor(Math.random() * 6) + 1;
}

const yesAnimated = (animationFrame) => {
  for (let i = 0; i < chars.length; i++) {
    chars[
      i
    ].src = `./assets/Graphics/TrumpYesSheet/trumpYesSpriteSheet_${animationFrame}.png`;
  }
};

const noAnimated = (animationFrame) => {
  for (let i = 0; i < chars.length; i++) {
    chars[
      i
    ].src = `./assets/Graphics/TrumpNoSheet/trumpNoSpriteSheet_${animationFrame}.png`;
  }
};

const setDice = (diceNumber1, diceNumber2) => {
  for (let i = 0; i < chars.length; i++) {
    greenDice[i].src = `./assets/Graphics/DiceGreen/G${diceNumber1}.png`;
    yellowDice[i].src = `./assets/Graphics/DiceYellow/Y${diceNumber2}.png`;
  }
};

let serverResponse = {
  code: "GAME_RESPONSE",
  result: {
    scores: [1, 0, 0, 0, 0, 0, 1, 0],
    dices: [4, 4],
    score: 0,
  },
};

const div = document.getElementsByClassName("button-container")[0];
let isAnimating = false;

div.addEventListener("click", async () => {
  const res = await getRollApi();
  serverResponse = res;
  userScore.textContent = serverResponse.result.score;
  if (isAnimating) return;
  isAnimating = true;

  // Run the dice animation for a few seconds
  const diceInterval = setInterval(() => {
    setDice(getRandomDiceNumber(), getRandomDiceNumber());
  }, 100);

  setTimeout(() => {
    clearInterval(diceInterval); // Stop the dice animation after a few seconds
    setDice(serverResponse.result.dices[0], serverResponse.result.dices[1]);

    let yesOrNoArray = serverResponse.result.scores;
    let animationFrame = 1;
    let animationsCompleted = 0;

    const animate = (animationFunction) => {
      animationFunction(animationFrame);
      animationFrame++;
      if (animationFrame <= 8) {
        setTimeout(() => animate(animationFunction), 100);
      } else {
        animationFrame = 1;
        animationsCompleted++;
        if (animationsCompleted === yesOrNoArray.length) {
          isAnimating = false;
        }
      }
    };

    yesOrNoArray.forEach((item, index) => {
      setTimeout(() => {
        if (item) {
          animate(yesAnimated);
          playAudio(yesAudio);
        } else {
          animate(noAnimated);
          playAudio(noAudio);
        }
      }, index * 1000); // Stagger the start of each animation
    });
  }, 2000); // Run the dice animation for 5 seconds
});
